package com.gitlab.visutrb.androsstore.api

import com.gitlab.visutrb.androsstore.api.exception.NeedsBrowserException
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import com.gitlab.visutrb.androsstore.api.util.DeviceConfig
import com.gitlab.visutrb.androsstore.api.util.PasswordEncrypter
import okhttp3.FormBody
import okhttp3.Headers
import okhttp3.OkHttpClient

class GoogleAuthApi(
    client: OkHttpClient,
    private val deviceConfig: DeviceConfig
) : BaseGoogleApi(client) {

    private val passwordEncrypter by lazy { PasswordEncrypter() }

    override val userAgentString: String
        get() = with(deviceConfig) { "GoogleAuth/1.4 ($device $buildId)" }

    fun login(
        email: String,
        password: String? = null,
        token: String? = null,
        deviceId: String? = null,
        service: Service
    ): GoogleAuthResponse {
        val form = generateLoginForm(email, password, token, deviceId, service.value)
        try {
            val response = httpPost(AUTH_URL, form, generateHeaders(service.appId))
            val body = response.body()!!.string()
            val map = convertResponseBodyToMap(body)
            return GoogleAuthResponse(
                token = map["Token"],
                auth = map["Auth"],
                email = map["Email"],
                firstName = map["firstName"],
                lastName = map["lastName"]
            )
        } catch (e: RequestFailureException) {
            val body = e.body ?: throw e
            val map = convertResponseBodyToMap(body)
            if (map.containsValue("NeedsBrowser")) {
                throw NeedsBrowserException(
                    message = map["ErrorDetails"] ?: "NeedsBrowser",
                    url = map["Url"]
                )
            } else throw e
        }
    }

    private fun generateLoginForm(
        email: String,
        password: String?,
        token: String?,
        deviceId: String?,
        service: String
    ) = FormBody.Builder().apply {
        addEncoded("accountType", "HOSTED_OR_GOOGLE")
        addEncoded("has_permission", "1")
        addEncoded("add_account", "1")
        addEncoded("source", "android")
        addEncoded("sdk_version", deviceConfig.sdkVersionInt.toString())
        addEncoded("caller_sig", GSF_SIGNATURE)
        addEncoded("clientSig", GSF_SIGNATURE)
        addEncoded("service", service)
        addEncoded("Email", email)
        deviceId?.let { addEncoded("androidId", it) }
        password?.let {
            addEncoded("EncryptedPasswd", passwordEncrypter.encryptPassword(email, it))
        }
        token?.let { addEncoded("Token", it) }
    }.build()

    private fun generateHeaders(appId: String) = Headers.Builder().apply {
        add("app", appId)
        add("user-agent", userAgentString)
    }.build()

    private fun convertResponseBodyToMap(body: String): Map<String, String> {
        val map = mutableMapOf<String, String>()
        for (line in body.split("\n")) {
            val kvp = line.split("=")
            map[kvp[0]] = kvp[1]
        }
        return map
    }

    enum class Service(val value: String, val appId: String) {
        AC2DM("ac2dm", "com.google.android.gms"),
        ANDROID_MARKET("androidmarket", "com.android.vending")
    }

    companion object {
        private const val AUTH_URL = "$BASE_URL/auth"
    }
}
