package com.gitlab.visutrb.androsstore.api.util

interface DeviceConfig {

    val sdkVersionInt: Int

    val fingerprint: String

    val manufacturer: String

    val carrierBranding: String

    val device: String

    val model: String

    val hardware: String

    val product: String

    val platformVersionRelease: String

    val buildId: String

    val locale: String

    val supportedLocales: List<String>

    val country: String

    val language: String

    val supportedAbis: List<String>

    val radioVersion: String

    val bootloaderVersion: String

    val networkOperator: String

    val simOperator: String

    val isRoaming: Boolean

    val isWideScreen: Boolean

    val touchScreenConfiguration: Int

    val keyboardConfiguration: Int

    val navigationConfiguration: Int

    val screenLayoutConfiguration: Int

    val isHardwareKeyboardHidden: Boolean

    val screenDensity: Int

    val screenWidth: Int

    val screenHeight: Int

    val glEsVersionInt: Int

    val glExtensions: List<String>

    val sharedLibraries: List<String>

    val systemFeatures: List<String>
}
