package com.gitlab.visutrb.androsstore.api

data class GoogleAuthResponse(
    val token: String?,
    val auth: String?,
    val email: String?,
    val firstName: String?,
    val lastName: String?
)
