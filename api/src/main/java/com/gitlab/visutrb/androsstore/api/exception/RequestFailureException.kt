package com.gitlab.visutrb.androsstore.api.exception

class RequestFailureException(val statusCode: Int, message: String, val body: String? = null) :
    Exception(message)
