package com.gitlab.visutrb.androsstore.api.exception

class NeedsBrowserException(message: String, val url: String?) : Exception(message) {
}
