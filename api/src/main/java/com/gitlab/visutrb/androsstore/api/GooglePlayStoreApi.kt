package com.gitlab.visutrb.androsstore.api

import android.util.Base64
import com.gitlab.visutrb.androsstore.proto.*
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import com.gitlab.visutrb.androsstore.api.util.DeviceConfig
import okhttp3.*
import okio.BufferedSource
import java.security.SecureRandom
import java.util.concurrent.TimeUnit

class GooglePlayStoreApi(
    client: OkHttpClient,
    private val deviceConfig: DeviceConfig,
    private val authToken: String,
    private val checkinToken: String,
    private val deviceId: String,
    private var deviceConfigToken: String? = null,
    private var cookie: String? = null
) : BaseGoogleApi(client) {

    override val userAgentString: String
        get() = with(deviceConfig) {
            "Android-Finsky/$PHONESKY_VERSION_NAME (" +
                    "api=3," +
                    "versionCode=$PHONESKY_VERSION_CODE," +
                    "sdk=$sdkVersionInt," +
                    "device=$device" +
                    "hardware=$hardware," +
                    "product=$product," +
                    "platformVersionRelease=$platformVersionRelease," +
                    "model=$model," +
                    "buildId=$buildId," +
                    "isWideScreen=$isWideScreen," +
                    "supportedAbis=${supportedAbis.joinToString(";")})"
        }

    fun uploadDeviceConfig(): UploadDeviceConfigResponse {
        val uploadDeviceConfigRequestProto = generateUploadDeviceConfigRequestProto()
        val requestBody = RequestBody.create(
            MediaType.parse(MIME_TYPE_PROTOBUF),
            uploadDeviceConfigRequestProto.toByteArray()
        )
        val response = httpPost(UPLOAD_DEVICE_CONFIG_URL, requestBody, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes())
            .payload.uploadDeviceConfigResponse
            .also {
                deviceConfigToken = it.uploadDeviceConfigToken
            }
    }

    fun getToc(): TocResponse {
        val url = HttpUrl.parse(TOC_URL)!!.newBuilder().apply {
            addQueryParameter("no_cache_isui", "true")
        }.build()
        val response = httpGet(url, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes())
            .payload.tocResponse.also { cookie = it.dfeCookie }
    }

    fun getTopChart(topChartCategory: TopChartCategory): List<DocV2> {
        val url = HttpUrl.parse(LIST_URL)!!.newBuilder().apply {
            addQueryParameter("c", "3")
            addQueryParameter("ctr", topChartCategory.value)
            topChartCategory.subCategory?.let { addQueryParameter("cat", it) }
        }.build()
        val response = httpGet(url, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes())
            .payload.listResponse.docList.first().childList.first().childList
    }

    fun getAppDetails(packageId: String): DetailsResponse {
        val url = HttpUrl.parse(DETAILS_URL)!!.newBuilder().apply {
            addEncodedQueryParameter("doc", packageId)
        }.build()
        val response = httpGet(url, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes()).payload.detailsResponse
    }

    fun search(query: String): List<DocV2> {
        val url = HttpUrl.parse(SEARCH_URL)!!.newBuilder().apply {
            addQueryParameter("q", query)
            addQueryParameter("c", "3")
        }.build()
        val response = httpGet(url, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes()).preFetchList.first()
            .response.payload.listResponse.docList.first()
            .childList.filter { it.docid.contains("apps") and it.docid.contains("search_results") }
    }

    fun acquire(doc: DocV2) {
        val url = HttpUrl.parse(ACQUIRE_URL)!!.newBuilder().apply {
            addQueryParameter("theme", "1")
        }.build()
        val acquireRequestProto = generateAcquireRequestProto(doc)
        val requestBody = RequestBody.create(
            MediaType.parse(MIME_TYPE_PROTOBUF),
            acquireRequestProto.toByteArray()
        )
        httpPost(url, requestBody, generateHeaders())
    }

    fun deliver(doc: DocV2): DeliveryResponse {
        val url = with(doc) {
            HttpUrl.parse(DELIVERY_URL)!!.newBuilder().apply {
                addQueryParameter("ot", offerList[0].offerType.toString())
                addQueryParameter("doc", docid)
                addQueryParameter("vc", details.appDetails.versionCode.toString())
            }.build()
        }
        val response = httpGet(url, generateHeaders())
        return ResponseWrapper.parseFrom(response.body()!!.bytes()).payload.deliveryResponse
    }

    fun getApkSource(appDeliveryData: AndroidAppDeliveryData): BufferedSource {
        var cookieString = ""
        for (cookie in appDeliveryData.downloadAuthCookieList) {
            cookieString += when {
                cookieString.isBlank() -> "${cookie.name}=${cookie.value}"
                else -> "; ${cookie.name}=${cookie.value}"
            }
        }

        val client = this.client.newBuilder()
            .readTimeout(1, TimeUnit.HOURS)
            .build()

        val headers = Headers.Builder().add("Cookie", cookieString).build()
        val request = Request.Builder()
            .get()
            .url(appDeliveryData.downloadUrl)
            .headers(headers)
            .build()
        val response = client.newCall(request).execute()
        if (!response.isSuccessful) {
            val bytes = response.body()!!.bytes()
            val serverCommands = ResponseWrapper.parseFrom(bytes).commands
            val errorMessage = serverCommands.displayErrorMessage
            throw RequestFailureException(statusCode = response.code(), message = errorMessage)
        }
        return response.body()!!.source()
    }

    private fun generateHeaders() = Headers.Builder().apply {
        add("user-agent", userAgentString)
        add("authorization", "GoogleLogin auth=$authToken")
        add("accept-language", deviceConfig.locale.replace("_", "-"))
        add("x-dfe-client-id", "am-unknown")
        add("x-dfe-encoded-targets", ENCODED_TARGETS)
        add("x-dfe-requestparams", "timeoutMs=35000")
        add("x-dfe-device-id", deviceId)
        add("x-dfe-device-checkin-consistency-token", checkinToken)
        add("x-dfe-network-type", "4")
        add("x-dfe-mccmnc", deviceConfig.simOperator)
        deviceConfigToken?.let { add("x-dfe-device-config-token", it) }
        cookie?.let { add("x-dfe-cookie", it) }
    }.build()

    private fun generateUploadDeviceConfigRequestProto() = with(deviceConfig) {
        val deviceConfigProto = DeviceConfigurationProto.newBuilder()
            .setTouchScreen(touchScreenConfiguration)
            .setKeyboard(keyboardConfiguration)
            .setNavigation(navigationConfiguration)
            .setScreenLayout(screenLayoutConfiguration)
            .setHasHardKeyboard(!isHardwareKeyboardHidden)
            .setHasFiveWayNavigation(navigationConfiguration != 1)
            .setScreenDensity(screenDensity)
            .setGlEsVersion(glEsVersionInt)
            .addAllSystemSharedLibrary(sharedLibraries)
            .addAllSystemAvailableFeature(systemFeatures)
            .addAllNativePlatform(supportedAbis)
            .setScreenWidth(screenWidth)
            .setScreenHeight(screenHeight)
            .addAllSystemSupportedLocale(supportedLocales)
            .addAllGlExtension(glExtensions)
            .build()
        UploadDeviceConfigRequest.newBuilder()
            .setManufacturer(manufacturer)
            .setDeviceConfiguration(deviceConfigProto)
            .build()
    }

    private fun generateAcquireRequestProto(doc: DocV2) = with(doc) {
        val acquireRequestDoc = AcquireRequestDoc.newBuilder()
            .setDocid(docid)
            .setDoctype(docType)
            .setBackendid(backendId)
            .build()
        val data = AcquireRequestData.newBuilder()
            .setDoc(acquireRequestDoc)
            .setUnknownAttr1(1)
            .setUnknownAttr2(1)
            .build()
        val acquireDetails = AcquireDetails.newBuilder()
            .setVersionCode(details.appDetails.versionCode)
            .build()
        val byteArray = ByteArray(256)
        val secureRandom = SecureRandom()
        secureRandom.nextBytes(byteArray)
        val randomStr = "nonce=" + Base64.encodeToString(byteArray, 11).toString()
        AcquireRequest.newBuilder()
            .setData(data)
            .setDetails(acquireDetails)
            .setUnknownAttr1(1)
            .setUnknownAttr2(randomStr)
            .setUnknownAttr3(1)
            .build()
    }

    enum class TopChartCategory(val value: String, val subCategory: String? = null) {
        TOP_FREE_APPS("apps_topselling_free_APPLICATION"),
        TOP_FREE_GAMES("apps_topselling_free", "GAME"),

        TOP_PAID_APPS("apps_topselling_paid_APPLICATION"),
        TOP_PAID_GAMES("apps_topselling_paid", "GAME"),

        TOP_GROSSING_APPS("apps_topgrossing_APPLICATION"),
        TOP_GROSSING_GAMES("apps_topgrossing", "GAME"),

        TRENDING_APPS("apps_movers_shakers"),
        TRENDING_GAMES("apps_movers_shakers", "GAME")
    }

    companion object {
        private const val FDFE_URL = "$BASE_URL/fdfe"

        private const val UPLOAD_DEVICE_CONFIG_URL = "$FDFE_URL/uploadDeviceConfig"
        private const val TOC_URL = "$FDFE_URL/toc"
        private const val LIST_URL = "$FDFE_URL/list"
        private const val DETAILS_URL = "$FDFE_URL/details"
        private const val SEARCH_SUGGEST_URl = "$FDFE_URL/searchSuggest"
        private const val SEARCH_URL = "$FDFE_URL/search"
        private const val ACQUIRE_URL = "$FDFE_URL/acquire"
        private const val DELIVERY_URL = "$FDFE_URL/delivery"

        private const val PHONESKY_VERSION_CODE = "81302300"
        private const val PHONESKY_VERSION_NAME = "13.0.23-all [0] [PR] 227930514"
    }
}
