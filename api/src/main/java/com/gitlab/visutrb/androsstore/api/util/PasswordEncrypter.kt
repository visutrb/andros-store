package com.gitlab.visutrb.androsstore.api.util

import android.util.Base64
import java.math.BigInteger
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.PublicKey
import java.security.spec.RSAPublicKeySpec
import javax.crypto.Cipher

/**
 * PasswordEncrypter, reconstructed from GoogleApps package.
 */
class PasswordEncrypter {

    /**
     * Encrypt password using Google's encryption strategy.
     *
     * @param username Google account username.
     * @param password Google account password.
     */
    fun encryptPassword(username: String, password: String): String {
        val input = "$username\u0000$password"
        val ciphertextHeader = ByteArray(5)
        val publicKey = createKey(ciphertextHeader)
        val cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA1ANDMGF1PADDING")
        val plaintext = input.toByteArray()
        val chunks = ((plaintext.size - 1) / 86) + 1
        val output = ByteArray(chunks * 133)

        var i = 0
        while (i < chunks) {
            cipher.init(1, publicKey)
            val ciphertext = cipher.doFinal(
                plaintext,
                i * 86,
                if (i == chunks + -1) plaintext.size - i * 86 else 86
            )
            System.arraycopy(
                ciphertextHeader,
                0,
                output,
                i * 133,
                ciphertextHeader.size
            )
            System.arraycopy(
                ciphertext,
                0,
                output,
                i * 133 + ciphertextHeader.size,
                ciphertext.size
            )
            i++
        }

        return Base64.encodeToString(output, 10)
    }

    private fun createKey(ciphertextHeader: ByteArray): PublicKey {
        var temp: ByteArray

        val binaryKey = Base64.decode(DEFAULT_PUBLIC_KEY, 0)
        val modulusLength = readInt(binaryKey, 0)
        temp = ByteArray(modulusLength)
        System.arraycopy(binaryKey, 4, temp, 0, modulusLength)

        val modulus = BigInteger(1, temp)
        val exponentLength = readInt(binaryKey, modulusLength + 4)
        temp = ByteArray(exponentLength)
        System.arraycopy(binaryKey, modulusLength + 8, temp, 0, exponentLength)

        val exponent = BigInteger(1, temp)
        val hash = MessageDigest.getInstance("SHA-1").digest(binaryKey)
        ciphertextHeader[0] = 0
        System.arraycopy(hash, 0, ciphertextHeader, 1, 4)

        return KeyFactory.getInstance("RSA")
            .generatePublic(RSAPublicKeySpec(modulus, exponent))
    }

    private fun readInt(src: ByteArray, offset: Int): Int {
        return 0 or
                ((src[offset].toInt() and 0xff) shl 24) or
                ((src[offset + 1].toInt() and 0xff) shl 16) or
                ((src[offset + 2].toInt() and 0xff) shl 8) or
                (src[offset + 3].toInt() and 0xff)
    }

    companion object {
        private const val DEFAULT_PUBLIC_KEY = "AAAAgMom/1a/v0lblO2Ubrt60J2gcuXSljGFQXgcyZWveWL" +
                "Ewo6prwgi3iJIZdodyhKZQrNWp5nKJ3srRXcUW+F1BD3baEVGcmEgqaLZUNBjm05" +
                "7pKRI16kB0YppeGx5qIQ5QjKzsR8ETQbKLNWgRY0QRNVz34kMJR3P/LgHax/6rmf" +
                "5AAAAAwEAAQ=="
    }
}
