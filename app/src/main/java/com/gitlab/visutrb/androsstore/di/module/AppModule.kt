package me.visutr.store.di.modules

import android.content.Context
import com.gitlab.visutrb.androsstore.App
import com.gitlab.visutrb.androsstore.di.scope.ApplicationScope
import com.gitlab.visutrb.androsstore.util.AppPrefs
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    @ApplicationScope
    fun appContext(app: App): Context  = app.applicationContext

    @Provides
    @ApplicationScope
    fun appPrefs(context: Context): AppPrefs = AppPrefs(context)
}
