package com.gitlab.visutrb.androsstore.util

import android.app.ActivityManager
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.telephony.TelephonyManager
import com.gitlab.visutrb.androsstore.api.util.DeviceConfig

class AndroidDeviceConfig(
    private val context: Context,
    private val glExtensionReader: GlExtensionReader
) : DeviceConfig {

    private val configuration
        get() = context.resources.configuration

    private val telephonyManager
        get() = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

    override val sdkVersionInt: Int
        get() = Build.VERSION.SDK_INT

    override val fingerprint: String
        get() = Build.FINGERPRINT

    override val manufacturer: String
        get() = Build.MANUFACTURER

    override val carrierBranding: String
        get() = Build.BRAND

    override val device: String
        get() = Build.DEVICE

    override val model: String
        get() = Build.MODEL

    override val hardware: String
        get() = Build.HARDWARE

    override val product: String
        get() = Build.PRODUCT

    override val platformVersionRelease: String
        get() = Build.VERSION.RELEASE

    override val buildId: String
        get() = Build.ID

    override val locale: String
        get() {
            return if (Build.VERSION.SDK_INT < 24) configuration.locale.toString()
            else configuration.locales[0].toString()
        }

    override val supportedLocales: List<String>
        get() = context.assets.locales.map { it.toString() }

    override val country: String
        get() {
            return if (Build.VERSION.SDK_INT < 24) configuration.locale.country.toString()
            else configuration.locales[0].country.toString()
        }

    override val language: String
        get() {
            return if (Build.VERSION.SDK_INT < 24) configuration.locale.language.toString()
            else configuration.locales[0].language.toString()
        }

    override val supportedAbis: List<String>
        get() {
            return if (Build.VERSION.SDK_INT < 21) listOf(Build.CPU_ABI)
            else Build.SUPPORTED_ABIS.asList()
        }

    override val radioVersion: String
        get() = Build.getRadioVersion()

    override val bootloaderVersion: String
        get() = Build.BOOTLOADER

    override val networkOperator: String
        get() = telephonyManager.networkOperator ?: ""

    override val simOperator: String
        get() = telephonyManager.simOperator ?: ""

    override val isRoaming: Boolean
        get() = telephonyManager.isNetworkRoaming

    override val isWideScreen: Boolean
        get() = configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

    override val touchScreenConfiguration: Int
        get() = configuration.touchscreen

    override val keyboardConfiguration: Int
        get() = configuration.keyboard

    override val navigationConfiguration: Int
        get() = configuration.navigation

    override val screenLayoutConfiguration: Int
        get() = configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK

    override val isHardwareKeyboardHidden: Boolean
        get() = configuration.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES

    override val screenDensity: Int
        get() = context.resources.displayMetrics.densityDpi

    override val screenWidth: Int
        get() = context.resources.displayMetrics.widthPixels

    override val screenHeight: Int
        get() = context.resources.displayMetrics.heightPixels

    override val glEsVersionInt: Int
        get() {
            val activityManager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            return activityManager.deviceConfigurationInfo.reqGlEsVersion
        }

    override val glExtensions: List<String>
        get() = glExtensionReader.getGlExtensions()

    override val sharedLibraries: List<String>
        get() = context.packageManager.systemSharedLibraryNames.sorted()

    override val systemFeatures: List<String>
        get() = context.packageManager.systemAvailableFeatures
            .filter { it.name != null }
            .map { it.name }
            .sorted()
}
