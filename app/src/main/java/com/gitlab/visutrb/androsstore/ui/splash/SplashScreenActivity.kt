package com.gitlab.visutrb.androsstore.ui.splash

import android.content.Intent
import android.os.Bundle
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.ui.BaseActivity
import com.gitlab.visutrb.androsstore.ui.login.LoginActivity
import com.gitlab.visutrb.androsstore.ui.main.MainActivity
import com.gitlab.visutrb.androsstore.util.AppPrefs
import javax.inject.Inject

class SplashScreenActivity : BaseActivity() {

    @Inject lateinit var appPrefs: AppPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val intent = when (appPrefs.isLoggedIn) {
            true -> Intent(this, MainActivity::class.java)
            false -> Intent(this, LoginActivity::class.java)
        }
        startActivity(intent)
    }
}
