package com.gitlab.visutrb.androsstore.ui.main.fragment

import com.gitlab.visutrb.androsstore.R

class ExploreFragment : BaseTopChartsFragment(TopChartType.APPLICATION) {

    override val topFreeTitle: String
        get() = getString(R.string.explore_top_free_apps_icap)

    override val topPaidTitle: String
        get() = getString(R.string.explore_top_paid_apps_icap)

    override val topGrossingTitle: String
        get() = getString(R.string.explore_top_grossing_apps_icap)

    companion object {
        fun newInstance() = ExploreFragment()
    }
}
