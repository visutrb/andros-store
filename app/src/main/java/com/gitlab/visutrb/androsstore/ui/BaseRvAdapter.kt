package com.gitlab.visutrb.androsstore.ui

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseRvAdapter<Item> : RecyclerView.Adapter<BaseRvAdapter.ViewHolder>() {

    private var _items = mutableListOf<Item>()

    protected val items: List<Item>
        get() = _items

    override fun getItemCount(): Int = _items.size

    fun add(item: Item) {
        val insertPosition = _items.size
        _items.add(item)
        notifyItemInserted(insertPosition)
    }

    fun addAll(items: List<Item>) {
        val insertPosition = _items.size
        _items.addAll(items)
        notifyItemRangeInserted(insertPosition, items.size)
    }

    fun replaceAt(position: Int, item: Item) {
        _items[position] = item
        notifyItemChanged(position)
    }

    fun replaceAll(items: List<Item>) {
        _items = items.toMutableList()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
