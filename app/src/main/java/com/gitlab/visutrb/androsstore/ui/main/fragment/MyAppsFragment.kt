package com.gitlab.visutrb.androsstore.ui.main.fragment

import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.proto.Offer
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.ui.BaseFragment
import com.gitlab.visutrb.androsstore.ui.appdetails.AppDetailsIntentHelper
import com.gitlab.visutrb.androsstore.ui.main.view.LocalAppInfoView
import kotlinx.android.synthetic.main.layout_installed_applications.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MyAppsFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_apps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showInstalledAppsAsync()
    }

    private fun showInstalledAppsAsync() = launch {
        val pm = context!!.packageManager
        val flag = PackageManager.GET_META_DATA
        val appInfoList = withContext(IO) {
            pm.getInstalledApplications(flag)
                .filter { (it.name != "null" || it.name != null) }
                .filter { (it.flags and ApplicationInfo.FLAG_SYSTEM) == 0 }
                .sortedBy { it.loadLabel(pm).toString() }
        }
        for (appInfo in appInfoList) {
            val packageName = appInfo.packageName
            val appLabel = appInfo.loadLabel(pm).toString()
            val icon = withContext(IO) { appInfo.loadIcon(pm) }
            val isLaunchable = withContext(IO) { pm.getLaunchIntentForPackage(packageName) != null }
            val view = LocalAppInfoView(context!!).apply {
                setAppInfo(packageName, appLabel, icon, isLaunchable)
                onLaunchBtnClick = { launchApplication(it) }
                onItemClick = { showAppDetailsActivity(it) }
            }
            installedAppsContainer.addView(view)
        }
    }

    private fun launchApplication(packageName: String) {
        val launchIntent = context!!.packageManager.getLaunchIntentForPackage(packageName)
        launchIntent?.let { startActivity(it) }
    }

    private fun showAppDetailsActivity(packageName: String) {
        val offer = Offer.newBuilder()
            .setMicros(1)
            .build()
        val doc = DocV2.newBuilder()
            .setDocid(packageName)
            .addOffer(offer)
            .build()
        startActivity(AppDetailsIntentHelper.newIntent(context = activity!!, doc = doc))
    }

    companion object {
        fun newInstance() = MyAppsFragment()
    }
}
