package com.gitlab.visutrb.androsstore.util

import android.opengl.GLES10
import android.text.TextUtils
import android.util.Log
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.egl.EGLDisplay

class GlExtensionReader {

    fun getGlExtensions(): List<String> {
        val extensionsSet = hashSetOf<String>()
        val egl10 = EGLContext.getEGL() as EGL10
        if (egl10 == null) {
            Log.d(TAG, "Cannot get EGL10, returning an empty list.")
            return listOf()
        }

        val eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY)
        egl10.eglInitialize(eglGetDisplay, IntArray(2))

        val numConfig = IntArray(1)
        if (egl10.eglGetConfigs(eglGetDisplay, null, 0, numConfig)) {
            val eglConfigArr = arrayOfNulls<EGLConfig>(numConfig[0])
            if (egl10.eglGetConfigs(eglGetDisplay, eglConfigArr, numConfig[0], numConfig)) {
                val surfaceSizeArgs = intArrayOf(
                    EGL10.EGL_WIDTH,
                    EGL10.EGL_PBUFFER_BIT,
                    EGL10.EGL_HEIGHT,
                    EGL10.EGL_PBUFFER_BIT,
                    EGL10.EGL_NONE
                )

                val ctxAttrs = intArrayOf(12440, EGL10.EGL_PIXMAP_BIT, EGL10.EGL_NONE)
                val value = IntArray(1)
                for (i in 0 until numConfig[0]) {
                    egl10.eglGetConfigAttrib(
                        eglGetDisplay,
                        eglConfigArr[i],
                        EGL10.EGL_CONFIG_CAVEAT,
                        value
                    )
                    if (value[0] != EGL10.EGL_SLOW_CONFIG) {
                        egl10.eglGetConfigAttrib(
                            eglGetDisplay,
                            eglConfigArr[i],
                            EGL10.EGL_SURFACE_TYPE,
                            value
                        )
                        if (value[0] and EGL10.EGL_PBUFFER_BIT != 0) {
                            egl10.eglGetConfigAttrib(
                                eglGetDisplay,
                                eglConfigArr[i],
                                EGL10.EGL_RENDERABLE_TYPE,
                                value
                            )
                            if (value[0] and EGL10.EGL_PBUFFER_BIT != 0) {
                                addExtensionsForConfig(
                                    egl10,
                                    eglGetDisplay,
                                    eglConfigArr[i],
                                    surfaceSizeArgs,
                                    null,
                                    extensionsSet
                                )
                            }
                            if (value[0] and EGL10.EGL_WINDOW_BIT != 0) {
                                addExtensionsForConfig(
                                    egl10,
                                    eglGetDisplay,
                                    eglConfigArr[i],
                                    surfaceSizeArgs,
                                    ctxAttrs,
                                    extensionsSet
                                )
                            }
                        }
                    }
                }
                egl10.eglTerminate(eglGetDisplay)
            }
        }

        return extensionsSet.toSortedSet().toList()
    }

    private fun addExtensionsForConfig(
        egl: EGL10,
        display: EGLDisplay,
        config: EGLConfig?,
        surfaceSize: IntArray,
        contextAttrs: IntArray?,
        glExtensions: MutableSet<String>
    ) {
        val context = egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, contextAttrs)
        if (context !== EGL10.EGL_NO_CONTEXT) {
            val surface = egl.eglCreatePbufferSurface(display, config, surfaceSize)
            if (surface === EGL10.EGL_NO_SURFACE) {
                egl.eglDestroyContext(display, context)
                return
            }
            egl.eglMakeCurrent(display, surface, surface, context)
            val extensionList = GLES10.glGetString(GLES10.GL_EXTENSIONS)
            if (!TextUtils.isEmpty(extensionList)) {
                for (extension in extensionList.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                    glExtensions.add(extension)
                }
            }
            egl.eglMakeCurrent(
                display,
                EGL10.EGL_NO_SURFACE,
                EGL10.EGL_NO_SURFACE,
                EGL10.EGL_NO_CONTEXT
            )
            egl.eglDestroySurface(display, surface)
            egl.eglDestroyContext(display, context)
        }
    }

    companion object {
        private const val TAG = "GlExtensionReader"
    }
}
