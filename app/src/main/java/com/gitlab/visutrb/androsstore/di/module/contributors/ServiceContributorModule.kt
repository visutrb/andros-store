package com.gitlab.visutrb.androsstore.di.module.contributors

import com.gitlab.visutrb.androsstore.di.scope.ApplicationScope
import com.gitlab.visutrb.androsstore.di.scope.ServiceScope
import com.gitlab.visutrb.androsstore.service.DownloadManagerService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceContributorModule {

    @ServiceScope
    @ContributesAndroidInjector
    abstract fun downloadManagerService(): DownloadManagerService
}
