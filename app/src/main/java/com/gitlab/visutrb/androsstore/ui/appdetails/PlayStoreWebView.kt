package com.gitlab.visutrb.androsstore.ui.appdetails

import android.content.Context
import android.graphics.Bitmap
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient

class PlayStoreWebView : WebView {

    var onPageStarted: ((url: String?) -> Unit)? = null
    var onPageFinished: ((url: String?) -> Unit)? = null

    var onPurchaseCancelled: (() -> Unit)? = null
    var onPurchaseCompleted: (() -> Unit)? = null

    private lateinit var packageId: String

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
            context,
            attrs,
            defStyleAttr
    )

    fun loadDetailsPage(packageId: String) {
        this.packageId = packageId
        val client = PlayStoreWebViewClient()
        settings.apply {
            javaScriptEnabled = true
            domStorageEnabled = true
            webViewClient = client
            webChromeClient = LoggableWebChromeClient()
        }

        removeJavascriptInterface(TAG_JS_INTERFACE)
        addJavascriptInterface(client, TAG_JS_INTERFACE)

        loadUrl(GOOGLE_PLAY_AUTH_URL + packageId)
    }

    private fun hideAppDetails() {
        val js = "(function() {" +
                "  document.querySelector('div').style.visibility = 'hidden';" +
                "  setInterval(function() {" +
                "    $TAG_JS_INTERFACE.onPageReady();" +
                "  }, 500);" +
                "})"
        executeJs(js)
    }

    private fun registerMutationObserver() {
        val frameSelector = "iframe[src*=\"/store/epurchase\"]"
        val dialogSelector = "div[class=\"websky-modal-dialog\"]"
        val cancelBtn1Selector = "button[id=\"purchase-cancel-button\"]"
        val cancelBtn2Selector = "button[id=\"loonie-cancel-purchase-button\"]"
        val purchaseCompleteContainerSelector = "[class=\"purchase-complete-container\"]"
        val closeDialogBtn = "[id=\"close-dialog-button\"]"

        val obvrOptions = "{ attributes: true, childList: true, subtree: true }"

        val js = "(function() {" +
                "  var frameMutationCallback = function() {" +
                "    var frame = document.querySelector('$frameSelector');" +
                "    var innerDoc = frame.contentDocument;" +
                "    var dialog = innerDoc.querySelector('$dialogSelector');" +
                "    var cancelBtn1 = innerDoc.querySelector('$cancelBtn1Selector');" +
                "    var cancelBtn2 = innerDoc.querySelector('$cancelBtn2Selector');" +
                "    var purchaseCompleteContainer = innerDoc.querySelector('$purchaseCompleteContainerSelector');" +
                "    var closeDialogBtn = innerDoc.querySelector('$closeDialogBtn');" +

                "    if (dialog) {" +
                "      dialog.style.boxShadow = '0 0';" +
                "    }" +

                "    if (cancelBtn1) {" +
                "      cancelBtn1.addEventListener('click', function() {" +
                "        $TAG_JS_INTERFACE.onPurchaseCancelled();" +
                "      });" +
                "    }" +

                "    if (cancelBtn2) {" +
                "      cancelBtn2.addEventListener('click', function() {" +
                "        $TAG_JS_INTERFACE.onPurchaseCancelled();" +
                "      });" +
                "    }" +

                "    if (purchaseCompleteContainer) {" +
                "      purchaseCompleteContainer.style.visibility = 'hidden';" +
                "    }" +

                "    if (closeDialogBtn) {" +
                "      $TAG_JS_INTERFACE.onPurchaseCompleted();" +
                "      closeDialogBtn.click();" +
                "    }" +
                "  };" +

                "  var docMutationCallback = function() {" +
                "    var frame = document.querySelector('$frameSelector');" +
                "    if (frame) {" +
                "      var observer = new MutationObserver(frameMutationCallback);" +
                "      observer.observe(frame.contentDocument, $obvrOptions);" +
                "    }" +
                "  };" +

                "  var observer = new MutationObserver(docMutationCallback);" +
                "  observer.observe(document, $obvrOptions);" +
                "})"

        executeJs(js)
    }

    private fun showCheckoutDialog() {
        val btn1Selector = "button[data-item-id*=\"$packageId\"]"
        val js = "(function() { document.querySelector('$btn1Selector').click(); })"
        executeJs(js)
    }

    private fun executeJs(js: String) {
        loadUrl("javascript:$js()")
    }

    private inner class PlayStoreWebViewClient : WebViewClient() {

        private var isFirstLoad = true
        private var shouldShowCheckOutDialog = true

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            onPageStarted?.invoke(url)

            if (url?.startsWith(APP_DETAILS_URL) == true) {
                visibility = View.INVISIBLE
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            onPageFinished?.invoke(url)

            if (url?.startsWith(APP_DETAILS_URL) == true) {
                hideAppDetails()
                registerMutationObserver()
                if (shouldShowCheckOutDialog) {
                    showCheckoutDialog()
                    shouldShowCheckOutDialog = false
                }
            }
        }

        @JavascriptInterface
        fun onPageReady() {
            Handler(Looper.getMainLooper()).post { visibility = View.VISIBLE }
        }

        @JavascriptInterface
        fun onPurchaseCancelled() {
            onPurchaseCancelled?.invoke()
        }

        @JavascriptInterface
        fun onPurchaseCompleted() {
            onPurchaseCompleted?.invoke()
        }

        @JavascriptInterface
        fun resetFirstLoadState() {
            isFirstLoad = true
        }
    }

    companion object {
        private const val TAG_JS_INTERFACE = "PlayStoreJsInterface"

        private const val APP_DETAILS_URL = "https://play.google.com/store/apps/details"
        private const val GOOGLE_ACCOUNTS_URL = "https://accounts.google.com"
        private const val GOOGLE_PLAY_AUTH_URL = "$GOOGLE_ACCOUNTS_URL/ServiceLogin" +
                "?service=googleplay&passive=86400" +
                "&continue=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3D"
    }
}
