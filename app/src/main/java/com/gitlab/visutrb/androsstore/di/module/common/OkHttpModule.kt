package com.gitlab.visutrb.androsstore.di.module.common

import android.content.Context
import com.gitlab.visutrb.androsstore.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import java.util.concurrent.TimeUnit

@Module
class OkHttpModule {

    @Provides
    @ApplicationScope
    fun cache(context: Context): Cache {
        val cacheSize = 20971520 // 20MB
        return Cache(context.cacheDir, cacheSize.toLong())
    }

    @Provides
    @ApplicationScope
    fun okHttpClient(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
            .cache(cache)
            .addInterceptor(HttpLoggingInterceptor().apply { level = Level.BASIC })
            .addNetworkInterceptor { chain ->
                val request = chain.request()
                val cacheControl = request.cacheControl() ?: CacheControl.Builder()
                    .maxAge(3, TimeUnit.DAYS)
                    .build()
                chain.proceed(request.newBuilder().cacheControl(cacheControl).build())
            }
            .build()
    }
}
