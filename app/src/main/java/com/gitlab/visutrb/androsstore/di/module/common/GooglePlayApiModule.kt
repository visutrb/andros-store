package com.gitlab.visutrb.androsstore.di.module.common

import android.content.Context
import com.gitlab.visutrb.androsstore.api.GoogleAuthApi
import com.gitlab.visutrb.androsstore.api.GoogleCheckinApi
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.api.util.DeviceConfig
import com.gitlab.visutrb.androsstore.di.scope.ApplicationScope
import com.gitlab.visutrb.androsstore.util.AndroidDeviceConfig
import com.gitlab.visutrb.androsstore.util.AppPrefs
import com.gitlab.visutrb.androsstore.util.GlExtensionReader
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class GooglePlayApiModule {

    @Provides
    @ApplicationScope
    fun glExtensionReader(): GlExtensionReader {
        return GlExtensionReader()
    }

    @Provides
    @ApplicationScope
    fun deviceConfig(context: Context, glExtensionReader: GlExtensionReader): DeviceConfig {
        return AndroidDeviceConfig(context, glExtensionReader)
    }

    @Provides
    @ApplicationScope
    fun googleAuthApi(okHttpClient: OkHttpClient, deviceConfig: DeviceConfig): GoogleAuthApi {
        return GoogleAuthApi(okHttpClient, deviceConfig)
    }

    @Provides
    @ApplicationScope
    fun googleCheckinApi(
        okHttpClient: OkHttpClient,
        deviceConfig: DeviceConfig
    ): GoogleCheckinApi {
        return GoogleCheckinApi(okHttpClient, deviceConfig)
    }

    @Provides
    @ApplicationScope
    fun googlePlayStoreApi(
        okHttpClient: OkHttpClient,
        deviceConfig: DeviceConfig,
        appPrefs: AppPrefs
    ): GooglePlayStoreApi {
        return GooglePlayStoreApi(
            okHttpClient, deviceConfig,
            authToken = appPrefs.authToken ?: "",
            checkinToken = appPrefs.checkinToken ?: "",
            deviceId = appPrefs.deviceId ?: ""
        )
    }
}
