package com.gitlab.visutrb.androsstore.ui.main.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.FrameLayout
import com.gitlab.visutrb.androsstore.R
import kotlinx.android.synthetic.main.view_local_app_info.view.*

class LocalAppInfoView(context: Context) : FrameLayout(context) {

    var onLaunchBtnClick: ((pkgName: String) -> Unit)? = null
    var onItemClick: ((pkgName: String) -> Unit)? = null

    init {
        inflateViews()
    }

    private fun inflateViews() {
        View.inflate(context, R.layout.view_local_app_info, this)
    }

    fun setAppInfo(
        packageName: String,
        applicationLabel: String,
        icon: Drawable?,
        isLaunchable: Boolean
    ) {
        appContainer.setOnClickListener { onItemClick?.invoke(packageName) }
        appNameTv.apply {
            text = applicationLabel
            setBackgroundResource(android.R.color.transparent)
        }
        launchAppBtn.apply {
            isEnabled = isLaunchable
            visibility = if (isLaunchable) View.VISIBLE else View.INVISIBLE
            setOnClickListener { onLaunchBtnClick?.invoke(packageName) }
        }
        appIconImg.setImageDrawable(icon)
    }

}
