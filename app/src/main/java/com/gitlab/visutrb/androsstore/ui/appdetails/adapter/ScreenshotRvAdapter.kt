package com.gitlab.visutrb.androsstore.ui.appdetails.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.gitlab.visutrb.androsstore.proto.Image
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.ui.BaseRvAdapter
import kotlinx.android.synthetic.main.item_app_details_screenshot.view.*

class ScreenshotRvAdapter : BaseRvAdapter<Image>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_app_details_screenshot, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val screenshot = items[position]
        with(holder.itemView) {
            Glide.with(context).load(screenshot.imageUrl).into(appDetailsScreenshotImv)
        }
    }
}
