package com.gitlab.visutrb.androsstore.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.support.v4.content.FileProvider
import com.gitlab.visutrb.androsstore.BuildConfig
import java.io.File

object PackageHelper {

    private const val APK_MIME_TYPE = "application/vnd.android.package-archive"

    fun getApkPath(
        packageName: String,
        context: Context
    ): File {
        val apkDir = context.getExternalFilesDir("apk")
        return File(apkDir, "$packageName.apk")
    }

    fun getInstallIntent(
        packageName: String,
        context: Context
    ): Intent {
        val apkPath = getApkPath(packageName, context)
        val uri = when (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            true -> FileProvider.getUriForFile(
                context,
                "${BuildConfig.APPLICATION_ID}.provider",
                apkPath
            )
            false -> Uri.fromFile(apkPath)
        }
        return Intent().apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                action = Intent.ACTION_INSTALL_PACKAGE
                data = uri
                addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            } else {
                action = Intent.ACTION_VIEW
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                setDataAndType(uri, APK_MIME_TYPE)
            }
        }
    }

    fun getUninstallIntent(packageName: String): Intent {
        return Intent(Intent.ACTION_DELETE)
            .setData(Uri.parse("package:$packageName"))
    }

    fun deleteApk(packageName: String, context: Context) {
        val apk = getApkPath(packageName, context)
        apk.delete()
    }

    fun isDownloaded(packageName: String, context: Context): Boolean {
        return getApkPath(packageName, context).exists()
    }
}
