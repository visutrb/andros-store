package com.gitlab.visutrb.androsstore.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import com.gitlab.visutrb.androsstore.ui.BaseActivity
import com.gitlab.visutrb.androsstore.ui.disableShiftMode
import com.gitlab.visutrb.androsstore.ui.main.fragment.ExploreFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.GamesFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.MyAppsFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.SettingsFragment
import com.gitlab.visutrb.androsstore.ui.search.SearchResultsActivity
import com.gitlab.visutrb.androsstore.util.AppPrefs
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_appbar_search.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainActivity : BaseActivity(), HasSupportFragmentInjector {

    private var currentFragmentTag: String? = null

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject lateinit var appPrefs: AppPrefs
    @Inject lateinit var googlePlayStoreApi: GooglePlayStoreApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    override fun onResume() {
        super.onResume()
        initAppAsync()
    }

    private fun initViews() {
        bottomNav.apply {
            disableShiftMode()
            setOnNavigationItemSelectedListener { onNavigationItemSelectedListener(it); true }
        }

        appbarSearch.setOnQueryTextListener(QueryTextListener())
    }

    private fun initAppAsync() = launch {
        if (appPrefs.isDeviceConfigUploaded) {
            showFragmentByTag(currentFragmentTag ?: FRAGMENT_EXPLORE)
        } else try {
            val udcResponse = withContext(IO) { googlePlayStoreApi.uploadDeviceConfig() }
            appPrefs.apply {
                isDeviceConfigUploaded = true
                deviceConfigToken = udcResponse.uploadDeviceConfigToken
            }
            showFragmentByTag(currentFragmentTag ?: FRAGMENT_EXPLORE)
        } catch (e: RequestFailureException) {
            Toast.makeText(
                this@MainActivity,
                "Cannot upload device config",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun onNavigationItemSelectedListener(menuItem: MenuItem) {
        when (menuItem.itemId) {
            R.id.menu_explore -> showFragmentByTag(FRAGMENT_EXPLORE)
            R.id.menu_games -> showFragmentByTag(FRAGMENT_GAMES)
            R.id.menu_my_apps -> showFragmentByTag(FRAGMENT_MY_APPS)
            // R.id.menu_settings -> showFragmentByTag(FRAGMENT_SETTINGS)
        }
    }

    private fun showFragmentByTag(tag: String) {
        var addFragment = false
        val fragment = supportFragmentManager.findFragmentByTag(tag) ?: run {
            addFragment = true
            when (tag) {
                FRAGMENT_EXPLORE -> ExploreFragment.newInstance()
                FRAGMENT_GAMES -> GamesFragment.newInstance()
                FRAGMENT_MY_APPS -> MyAppsFragment.newInstance()
                FRAGMENT_SETTINGS -> SettingsFragment.newInstance()
                else -> return
            }
        }
        supportFragmentManager.beginTransaction().run {
            if (addFragment) add(mainContentView.id, fragment, tag)
            supportFragmentManager.fragments.filter { it.tag != tag }.forEach { hide(it) }
            show(fragment)
            commit()
            currentFragmentTag = tag
        }
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> = fragmentInjector

    private inner class QueryTextListener :
        android.support.v7.widget.SearchView.OnQueryTextListener {

        override fun onQueryTextSubmit(query: String?): Boolean {
            query ?: return false
            SearchResultsActivity.newIntent(this@MainActivity, query)
                .also { startActivity(it) }
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            return true
        }
    }

    companion object {
        fun newIntent(context: Context): Intent = Intent(context, MainActivity::class.java)

        private const val FRAGMENT_EXPLORE = "main_fragment_explore"
        private const val FRAGMENT_GAMES = "main_fragment_game"
        private const val FRAGMENT_MY_APPS = "main_fragment_my_apps"
        private const val FRAGMENT_SETTINGS = "main_fragment_settings"
    }
}
