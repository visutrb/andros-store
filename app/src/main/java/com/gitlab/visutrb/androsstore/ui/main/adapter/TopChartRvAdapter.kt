package com.gitlab.visutrb.androsstore.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.ui.BaseRvAdapter
import kotlinx.android.synthetic.main.item_top_chart.view.*

class TopChartRvAdapter : BaseRvAdapter<DocV2>() {

    var onItemSelected: ((doc: DocV2, iconView: View) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_top_chart, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val doc = items[position]
        val appIconImage = doc.imageList.find { it.imageType == 4 }
        with(holder.itemView) {
            Glide.with(this)
                .asBitmap()
                .load(appIconImage?.imageUrl)
                .into(topChartAppIconImv)
            topChartAppTitle.text = doc.title
            setOnClickListener { onItemSelected?.invoke(doc, topChartAppIconImv) }
        }
    }
}
