package com.gitlab.visutrb.androsstore.di.module.contributors

import com.gitlab.visutrb.androsstore.di.scope.ActivityScope
import com.gitlab.visutrb.androsstore.ui.appdetails.FreeAppDetailsActivity
import com.gitlab.visutrb.androsstore.ui.appdetails.PaidAppDetailsActivity
import com.gitlab.visutrb.androsstore.ui.login.LoginActivity
import com.gitlab.visutrb.androsstore.ui.main.MainActivity
import com.gitlab.visutrb.androsstore.ui.search.SearchResultsActivity
import com.gitlab.visutrb.androsstore.ui.splash.SplashScreenActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityContributorModule {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun splashScreenActivity(): SplashScreenActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun loginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainFragmentsContributorModule::class])
    abstract fun mainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun freeAppDetailsActivity(): FreeAppDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun paidAppDetailsActivity(): PaidAppDetailsActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun searchResultsActivity(): SearchResultsActivity
}
