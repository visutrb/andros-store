package com.gitlab.visutrb.androsstore.ui.search.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.ui.BaseRvAdapter
import kotlinx.android.synthetic.main.item_search_results.view.*

class SearchResultsRvAdapter : BaseRvAdapter<DocV2>() {

    var onItemSelected: ((doc: DocV2, iconView: View) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_results, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val doc = items[position]
        val appIcon = doc.imageList.find { it.imageType == 4 }
        with(holder.itemView) {
            searchResultsAppTitle.text = doc.title
            searchResultsAppCreator.text = doc.creator
            appIcon?.let { Glide.with(context).load(it.imageUrl).into(searchResultsAppIcon) }
            setOnClickListener { onItemSelected?.invoke(doc, searchResultsAppIcon) }
        }
    }
}
