package com.gitlab.visutrb.androsstore.ui.appdetails

import android.content.Context
import android.content.Intent
import com.gitlab.visutrb.androsstore.proto.DocV2

object AppDetailsIntentHelper {

    fun newIntent(context: Context, doc: DocV2): Intent {
        val micros = doc.getOffer(0).micros
        return if (micros > 0) {
            PaidAppDetailsActivity.newIntent(context, doc)
        } else {
            FreeAppDetailsActivity.newIntent(context, doc)
        }
    }
}
