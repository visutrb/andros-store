package com.gitlab.visutrb.androsstore.ui.main.fragment

import com.gitlab.visutrb.androsstore.R

class GamesFragment : BaseTopChartsFragment(TopChartType.GAME) {

    override val topFreeTitle: String
        get() = getString(R.string.games_top_free_games_icap)

    override val topPaidTitle: String
        get() = getString(R.string.games_top_paid_games_icap)

    override val topGrossingTitle: String
        get() = getString(R.string.games_top_grossing_games_icap)

    companion object {
        fun newInstance() = GamesFragment()
    }
}
