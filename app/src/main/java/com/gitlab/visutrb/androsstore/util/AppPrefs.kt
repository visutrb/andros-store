package com.gitlab.visutrb.androsstore.util

import android.content.Context

class AppPrefs(context: Context) {

    private val sharedPrefs = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

    var isLoggedIn: Boolean
        get() = sharedPrefs.getBoolean(KEY_IS_LOGGED_IN, false)
        set(value) = sharedPrefs.edit().putBoolean(KEY_IS_LOGGED_IN, value).apply()

    var authToken: String?
        get() = sharedPrefs.getString(KEY_AUTH_TOKEN, null)
        set(value) = sharedPrefs.edit().putString(KEY_AUTH_TOKEN, value).apply()

    var isCheckedIn: Boolean
        get() = sharedPrefs.getBoolean(KEY_IS_CHECKED_IN, false)
        set(value) = sharedPrefs.edit().putBoolean(KEY_IS_CHECKED_IN, value).apply()

    var checkinToken: String?
        get() = sharedPrefs.getString(KEY_CHECKIN_TOKEN, null)
        set(value) = sharedPrefs.edit().putString(KEY_CHECKIN_TOKEN, value).apply()

    var deviceId: String?
        get() = sharedPrefs.getString(KEY_DEVICE_ID, null)
        set(value) = sharedPrefs.edit().putString(KEY_DEVICE_ID, value).apply()

    var isDeviceConfigUploaded: Boolean
        get() = sharedPrefs.getBoolean(KEY_IS_DEVICE_CONFIG_UPLOADED, false)
        set(value) = sharedPrefs.edit().putBoolean(KEY_IS_DEVICE_CONFIG_UPLOADED, value).apply()

    var deviceConfigToken: String?
        get() = sharedPrefs.getString(KEY_DEVICE_CONFIG_TOKEN, null)
        set(value) = sharedPrefs.edit().putString(KEY_DEVICE_CONFIG_TOKEN, value).apply()

    companion object {
        private const val SHARED_PREF_NAME = "app_pref"
        private const val KEY_IS_CHECKED_IN = "is_checked_in"
        private const val KEY_IS_LOGGED_IN = "is_logged_in"
        private const val KEY_IS_DEVICE_CONFIG_UPLOADED = "is_device_config_uploaded"
        private const val KEY_AUTH_TOKEN = "auth_token"
        private const val KEY_CHECKIN_TOKEN = "checkin_token"
        private const val KEY_DEVICE_ID = "device_id"
        private const val KEY_DEVICE_CONFIG_TOKEN = "device_config_token"
    }
}
