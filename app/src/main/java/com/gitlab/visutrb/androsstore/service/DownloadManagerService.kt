package com.gitlab.visutrb.androsstore.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import com.gitlab.visutrb.androsstore.proto.AndroidAppDeliveryData
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.util.PackageHelper
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import okio.BufferedSink
import okio.BufferedSource
import okio.Okio
import java.io.File
import java.util.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import kotlin.math.roundToInt

class DownloadManagerService : BaseService(), CoroutineScope {

    @Inject lateinit var playStoreApi: GooglePlayStoreApi

    private var startId = -255
    private var notificationId = startId
    private lateinit var downloadJob: Job

    private var current: Pair<DocV2, AndroidAppDeliveryData>? = null
    private val pendingList = LinkedList<Pair<DocV2, AndroidAppDeliveryData>>()

    private var interrupted = false
    private val cancelDownloadBroadcastReceiver by lazy { CancelDownloadBroadcastReceiver() }

    private val lbm
        get() = LocalBroadcastManager.getInstance(applicationContext)

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        registerReceiver(
            cancelDownloadBroadcastReceiver,
            IntentFilter(ACTION_CANCEL_CURRENT_DOWNLOAD)
        )
    }

    override fun onBind(intent: Intent?): IBinder = Binder()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        this.startId = startId
        notificationId = startId
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(cancelDownloadBroadcastReceiver)
    }

    fun addToDownloadQueue(doc: DocV2, deliveryData: AndroidAppDeliveryData) {
        pendingList.add(Pair(doc, deliveryData))
        if (current == null) proceedNextDownload()
    }

    fun removeFromDownloadQueue(doc: DocV2) {
        pendingList.filter { it.first.docid == doc.docid }.forEach { pendingList.remove(it) }
        if (current?.first?.docid == doc.docid) cancelDownload()
        else broadcastDownloadInterrupted(doc.docid)
    }

    fun contains(doc: DocV2): Boolean {
        val foundPending = pendingList.find { it.first.docid == doc.docid } != null
        val foundDownloading = current?.first?.docid == doc.docid
        return foundPending || foundDownloading
    }

    private fun proceedNextDownload() {
        current = when (pendingList.isNotEmpty()) {
            true -> pendingList.remove()
            false -> null
        }
        if (current != null) downloadJob = download()
        else stopForeground(true)
    }

    private fun download() = launch(IO) {
        val appDeliveryData = current?.second ?: return@launch proceedNextDownload()
        val packageName = current?.first?.docid ?: return@launch proceedNextDownload()

        val apkDest = PackageHelper.getApkPath(packageName, applicationContext)
        val tmpApkDest = File("${apkDest.absolutePath}.part")

        showProgressNotificationInForeground(progress = 0F, indeterminate = true)

        var bufferedSource: BufferedSource? = null
        var bufferedSink: BufferedSink? = null
        try {
            bufferedSource = playStoreApi.getApkSource(appDeliveryData)
            bufferedSink = Okio.buffer(Okio.sink(tmpApkDest))

            var readBytes: Long
            var downloadedBytes = 0L
            val totalBytes = appDeliveryData.downloadSize.toFloat()

            do {
                readBytes = bufferedSource.read(bufferedSink.buffer(), 4096)
                downloadedBytes += readBytes
                bufferedSink.flush()
                showProgressNotificationInForeground(downloadedBytes / totalBytes)
            } while (readBytes != -1L && !interrupted)

            if (interrupted) {
                broadcastDownloadInterrupted()
                tmpApkDest.delete()
            } else {
                tmpApkDest.renameTo(apkDest)
                withContext(Main) { showDownloadFinishedNotification() }
                broadcastDownloadFinished()
            }

        } catch (e: CancellationException) {
        } catch (e: Exception) {
            broadcastDownloadFailed(e)
            e.printStackTrace()
        } finally {
            bufferedSink?.close()
            bufferedSource?.close()
            stopForeground(true)
            proceedNextDownload()
            interrupted = false
        }
    }

    private fun cancelDownload() {
        interrupted = true
        downloadJob.cancel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) return
        getSystemService(NotificationManager::class.java).createNotificationChannel(
            NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_LOW
            )
        )
    }

    private fun showProgressNotificationInForeground(
        progress: Float,
        indeterminate: Boolean = false
    ) {
        val notification = createDownloadProgressNotification(progress, indeterminate)
        startForeground(PROGRESS_NOTIFICATION_ID, notification)
        broadcastDownloadProgress(progress, indeterminate)
    }

    private fun showDownloadFinishedNotification() {
        val notification = createDownloadFinishedNotification()
        NotificationManagerCompat.from(applicationContext)
            .notify(generateNotificationId(), notification)
    }

    private fun createDownloadProgressNotification(
        progress: Float,
        indeterminate: Boolean = false
    ): Notification {
        val appTitle = current?.first?.title ?: ""
        val progressInt = (progress * 1000).roundToInt()
        val cancelIntent = Intent(ACTION_CANCEL_CURRENT_DOWNLOAD)
        val cancelPendingIntent = PendingIntent.getBroadcast(
            this,
            startId,
            cancelIntent,
            0
        )
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(appTitle)
            .setContentText(getString(R.string.download_manager_downloading))
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setProgress(1000, progressInt, indeterminate)
            .addAction(
                R.drawable.ic_clear_gray,
                getString(R.string.download_manager_cancel),
                cancelPendingIntent
            )
            .build()
    }

    private fun createDownloadFinishedNotification(): Notification {
        val appTitle = current?.first?.title ?: ""
        val installIntent = PackageHelper.getInstallIntent(
            packageName = current!!.first.docid,
            context = applicationContext
        )
        val pendingInstallIntent = PendingIntent.getActivity(
            this,
            0,
            installIntent,
            0
        )
        return NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getString(R.string.download_manager_downloaded_app_title, appTitle))
            .setContentText(getString(R.string.download_manager_application_downloaded))
            .setContentIntent(pendingInstallIntent)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setAutoCancel(true)
            .build()
    }

    private fun broadcastDownloadProgress(progress: Float, indeterminate: Boolean) {
        val intent = Intent(ACTION_DOWNLOAD_PROGRESS_CHANGE)
            .putExtra(EXTRA_DOC_ID, current?.first?.docid)
            .putExtra(EXTRA_PROGRESS, progress)
            .putExtra(EXTRA_PROGRESS_INDETERMINATE, indeterminate)
        lbm.sendBroadcast(intent)
    }

    private fun broadcastDownloadFinished() {
        val intent = Intent(ACTION_DOWNLOAD_FINISHED)
            .putExtra(EXTRA_DOC_ID, current?.first?.docid)
        lbm.sendBroadcast(intent)
    }

    private fun broadcastDownloadInterrupted(docId: String? = null) {
        val intent = Intent(ACTION_DOWNLOAD_INTERRUPTED)
            .putExtra(EXTRA_DOC_ID, docId ?: current?.first?.docid)
        lbm.sendBroadcast(intent)
    }

    private fun broadcastDownloadFailed(e: Exception) {
        val intent = Intent(ACTION_DOWNLOAD_FAILED)
            .putExtra(EXTRA_DOC_ID, current?.first?.docid)
            .putExtra(EXTRA_EXCEPTION, e)
        lbm.sendBroadcast(intent)
    }

    private fun generateNotificationId(): Int {
        val sharedPref = getSharedPreferences(
            SHARED_PREF_NAME_NOTIFICATION_ID,
            Context.MODE_PRIVATE
        )
        val id = sharedPref.getInt(SHARED_PREF_KEY_CURRENT_ID, 0) % PROGRESS_NOTIFICATION_ID
        sharedPref.edit().putInt(SHARED_PREF_KEY_CURRENT_ID, id).apply()
        return id
    }

    inner class Binder : android.os.Binder() {
        fun getService() = this@DownloadManagerService
    }

    private inner class CancelDownloadBroadcastReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val action = intent?.action ?: return
            when (action) {
                ACTION_CANCEL_CURRENT_DOWNLOAD -> cancelDownload()
            }
        }
    }

    /**
     * @property ACTION_DOWNLOAD_PROGRESS_CHANGE Local broadcast action: Notifies progress changes.
     * @property ACTION_DOWNLOAD_FINISHED Local broadcast action: Notifies download finished.
     * @property ACTION_DOWNLOAD_INTERRUPTED Local broadcast action: Notifies download interrupted.
     * @property ACTION_DOWNLOAD_FAILED Local broadcast action: Notifies download failed
     * @property EXTRA_DOC_ID Used as local broadcast message's String extra field for doc id.
     * @property EXTRA_PROGRESS Used as local broadcast message's float extra field for progress. The value is bound between 0 and 1.
     * @property EXTRA_PROGRESS_INDETERMINATE Used as local broadcast message's boolean extra field to indicate if the download is indeterminate or not.
     * @property EXTRA_EXCEPTION Used as local broadcast message's serializable extra field for exception thrown while downloading.
     */
    companion object {
        const val ACTION_DOWNLOAD_PROGRESS_CHANGE =
            "DownloadManagerService.ACTION_DOWNLOAD_PROGRESS_CHANGE"
        const val ACTION_DOWNLOAD_FINISHED = "DownloadManagerService.ACTION_DOWNLOAD_FINISHED"
        const val ACTION_DOWNLOAD_INTERRUPTED = "DownloadManagerService.ACTION_DOWNLOAD_INTERRUPTED"
        const val ACTION_DOWNLOAD_FAILED = "DownloadManagerService.ACTION_DOWNLOAD_FAILED"

        const val EXTRA_DOC_ID = "DownloadManagerService.EXTRA_DOC_ID"
        const val EXTRA_PROGRESS = "DownloadManagerService.EXTRA_PROGRESS"
        const val EXTRA_PROGRESS_INDETERMINATE =
            "DownloadManagerService.EXTRA_PROGRESS_INDETERMINATE"
        const val EXTRA_EXCEPTION = "DownloadManagerService.EXTRA_EXCEPTION"

        private const val TAG = "DownloadManagerService"

        private const val CHANNEL_ID = "download-manager-service"
        private const val CHANNEL_NAME = "Download Manager"

        private const val ACTION_CANCEL_CURRENT_DOWNLOAD =
            "DownloadManagerService.ACTION_CANCEL_CURRENT_DOWNLOAD"

        private const val PROGRESS_NOTIFICATION_ID = 3000

        private const val SHARED_PREF_NAME_NOTIFICATION_ID = "notification_id"
        private const val SHARED_PREF_KEY_CURRENT_ID = "current_id"

    }
}
