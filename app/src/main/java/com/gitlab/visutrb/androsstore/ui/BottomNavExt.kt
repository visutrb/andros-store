package com.gitlab.visutrb.androsstore.ui

import android.annotation.SuppressLint
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView

@SuppressLint("RestrictedApi")
fun BottomNavigationView.disableShiftMode() {
    val menuView = this.getChildAt(0) as BottomNavigationMenuView
    try {
        val shiftingModeField = menuView::class.java.getDeclaredField("mShiftingMode")
        shiftingModeField.run {
            isAccessible = true
            setBoolean(menuView, false)
            isAccessible = false
        }

        for (i in 0 until menuView.childCount) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView
            item.setShiftingMode(false)
            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: Exception) {
    }
}
