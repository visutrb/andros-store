package com.gitlab.visutrb.androsstore.util

import java.text.DecimalFormat

object FileSizeUtil {

    private const val gb = 1000000000
    private const val mb = 1000000
    private const val kb = 1000

    fun getHumanReadableSize(fileSizeBytes: Long): String {
        val decimalFormat = DecimalFormat("###.##")
        return when {
            fileSizeBytes >= gb -> decimalFormat.format(fileSizeBytes / gb) + " GB"
            fileSizeBytes >= mb -> decimalFormat.format(fileSizeBytes / mb) + " MB"
            fileSizeBytes >= kb -> decimalFormat.format(fileSizeBytes / kb) + " KB"
            else -> "$fileSizeBytes Bytes"
        }
    }
}
