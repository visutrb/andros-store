package com.gitlab.visutrb.androsstore.service

import android.app.Service
import dagger.android.AndroidInjection

abstract class BaseService : Service() {

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }
}
