#  Open Android Store


This project is an open-source implementation of the Google Play Store application that allows
Android users to download and purchase applications directly from the Google Play Store.

![](docs/resources/screenshots/ss-explore.png) &nbsp; ![](docs/resources/screenshots/ss-games.png)

**Disclaimer:** This project is in heavy development and was designed for educational purpose.
